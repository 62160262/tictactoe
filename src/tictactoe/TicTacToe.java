/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.awt.BorderLayout;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class TicTacToe {

//    /**
//     * @param args the command line arguments
//     */
    static char winner;
    static boolean isFinish = false;
    static int row, col;
    static int countturn;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };

    /**
     * @param args the command line arguments
     */
    static char player;

    static void showWelcome() {
        System.out.println("Welcom to OX Game");
    }

    static void showTable() {
        System.out.println(" 1 2 3 ");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + "");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println(" ");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        //Normal flow
        while (true) {
            System.out.println("Please input Row Col :");
//         String str = kb.next();
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
//          System.out.println("row: "+row +" col: "+col); Debug1
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;

            }
            System.out.println("Error : table row and col is not empty!!!");
        }
//        showTable();
    }

    static void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (player != table[row][col]) {
                return;

            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkrow() {
        for (int col = 0; col < 3; col++) {
            if (player != table[row][col]) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j--) {
                if (table[j][j]!= player) {
                    return;
                }
            }
            isFinish = true;
            winner = player;
        }
    

    }

    static void checkY() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0, k = 2; j < 3; j--) {
                if (table[j][k] != player) {
                    return;
                }
            }
            isFinish = true;
            winner = player;
        }
    }

    static void win() {
        checkcol();
        checkrow();
        checkX();
        checkY();
        checkDraw();

    }
    static void checkDraw(){
        if(countturn >= 8){
            winner ='D';
            isFinish = true;
        }
    }

    static void switchPlayer() {
        if (player == 'x') {
            player = 'o';
        } else {
            player = 'x';
        }
        countturn++;
        
    }

    static void showResult() {
        if(winner == 'D'){
            System.out.println("Draw");
        }else{
            System.out.println(winner +"win !!!!");
    }

    }

    static void showBye() {
//        System.out.println("Bye bye ......");
    }

    public static void main(String[] args) {
        // TODO code application logic here
        do {
            showWelcome();
            showTable();
            showTurn();
            input();
            checkX();
            checkY();
            checkwin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();
//        Scanner kb = new Scanner(System.in);
//        int counto = 0;
//	int countx = 0;
//        
//        System.out.println("Welcome to OX Game");
//        
//        char x1 = kb.next().charAt(0);
//        
//        System.out.println("X turn");
//        System.out.println("Please input Row Col: 1 1");
//        
//        if(x1 == 'x'){
//            countx++;    
//        }else {
//            if(x1 == 'o'){
//                counto++;
//            }
//        
    }

    private static void checkwin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
